import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SignUpDetailsPageRoutingModule } from './sign-up-details-routing.module';

import { SignUpDetailsPage } from './sign-up-details.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SignUpDetailsPageRoutingModule
  ],
  declarations: [SignUpDetailsPage]
})
export class SignUpDetailsPageModule {}
