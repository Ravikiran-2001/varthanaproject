import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage-angular';
import Chart from 'chart.js/auto';

@Component({
  selector: 'app-sign-up-details',
  templateUrl: './sign-up-details.page.html',
  styleUrls: ['./sign-up-details.page.scss'],
})
export class SignUpDetailsPage implements OnInit {
  todaySignups: any[] = [];
  lastTwoDaysSignups: any[] = [];
  today = new Date().toISOString().split('T')[0];
  constructor(private storage: Storage, private router: Router) {}

  async ngOnInit() {
    const signupData: Record<string, number> =
      await this.getTodaySignupCounts();
    console.log('cccc', signupData);
    this.generatePieChart(signupData);
    this.todaySignups = await this.getTodaySignups();
    this.lastTwoDaysSignups = await this.getLastTwoDaysSignups();
  }

  async getTodaySignups(): Promise<any[]> {
    const keys = await this.storage.keys();
    const signups: any[] = [];

    for (const key of keys) {
      if (key.startsWith('userData_')) {
        const userData = await this.storage.get(key);
        const signupDate = new Date(userData.date).toISOString().split('T')[0];
        if (signupDate === this.today) {
          signups.push(userData);
        }
      }
    }

    return signups;
  }

  async getLastTwoDaysSignups(): Promise<any[]> {
    const today = new Date();
    const yesterday = new Date(today);
    yesterday.setDate(today.getDate() - 1);

    const todayString = today.toISOString().split('T')[0];
    const yesterdayString = yesterday.toISOString().split('T')[0];

    const keys = await this.storage.keys();
    const signups: any[] = [];

    for (const key of keys) {
      if (key.startsWith('userData_')) {
        const userData = await this.storage.get(key);
        const signupDate = new Date(userData.date).toISOString().split('T')[0];
        if (signupDate === todayString || signupDate === yesterdayString) {
          signups.push(userData);
        }
      }
    }

    return signups;
  }
  async getTodaySignupCounts(): Promise<Record<string, number>> {
    const keys = await this.storage.keys();
    const signupData: Record<string, number> = {};

    for (const key of keys) {
      if (key.startsWith('userData_')) {
        const userData = await this.storage.get(key);
        const signupDate = userData.date.toISOString().split('T')[0];
        if (signupDate === this.today) {
          signupData[userData.email] = (signupData[userData.email] || 0) + 1;
        }
      }
    }

    return signupData;
  }

  generatePieChart(signupData: Record<string, number>) {
    const labels = Object.keys(signupData);
    const data = Object.values(signupData);

    new Chart('signupChart', {
      type: 'pie',
      data: {
        labels: labels,
        datasets: [
          {
            data: data,
            backgroundColor: [
              '#FF6384',
              '#36A2EB',
              '#FFCE56',
              '#8BC34A',
              '#9C27B0',
              '#795548',
            ],
          },
        ],
      },
    });
  }
}
