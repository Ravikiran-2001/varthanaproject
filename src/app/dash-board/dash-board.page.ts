import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage-angular';
@Component({
  selector: 'app-dash-board',
  templateUrl: './dash-board.page.html',
  styleUrls: ['./dash-board.page.scss'],
})
export class DashBoardPage implements OnInit {
  constructor(private router: Router, private storage: Storage) {}
  ngOnInit() {
    this.storage.get('isLoggedIn').then((isLoggedIn) => {
      if (!isLoggedIn) {
        this.router.navigate(['/login']);
      }
    });
  }

  showWeeklyPieChart() {
    this.router.navigate(['/sign-up-details']);
  }

  showFetchedData() {
    this.router.navigate(['/api-fetch']);
  }

  showLoanHistory() {
    this.router.navigate(['/loan-history-api']);
  }

  navigateToLogin() {
    this.router.navigate(['']);
  }

  async logout() {
    await this.storage.remove('isLoggedIn');
    this.router.navigate(['/login']);
  }
}
