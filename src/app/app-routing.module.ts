import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { LoggedInGuard } from './logged-in.guard';
const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  },
  {
    path: 'login',
    loadChildren: () =>
      import('./login/login.module').then((m) => m.LoginPageModule),
  },
  {
    path: 'signup',
    loadChildren: () =>
      import('./signup/signup.module').then((m) => m.SignupPageModule),
  },
  {
    path: 'dash-board/:email',
    loadChildren: () =>
      import('./dash-board/dash-board.module').then(
        (m) => m.DashBoardPageModule
      ),
    canActivate: [LoggedInGuard],
  },
  {
    path: 'sign-up-details',
    loadChildren: () =>
      import('./sign-up-details/sign-up-details.module').then(
        (m) => m.SignUpDetailsPageModule
      ),
    canActivate: [LoggedInGuard],
  },
  
  {
    path: 'api-fetch',
    loadChildren: () => import('./api-fetch/api-fetch.module').then( m => m.ApiFetchPageModule)
  },
  {
    path: 'loan-history-api',
    loadChildren: () => import('./loan-history-api/loan-history-api.module').then( m => m.LoanHistoryApiPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
