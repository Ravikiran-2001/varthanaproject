import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LoanHistoryApiPageRoutingModule } from './loan-history-api-routing.module';

import { LoanHistoryApiPage } from './loan-history-api.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LoanHistoryApiPageRoutingModule
  ],
  declarations: [LoanHistoryApiPage]
})
export class LoanHistoryApiPageModule {}
