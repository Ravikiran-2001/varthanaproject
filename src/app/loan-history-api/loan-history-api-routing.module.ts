import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoanHistoryApiPage } from './loan-history-api.page';

const routes: Routes = [
  {
    path: '',
    component: LoanHistoryApiPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LoanHistoryApiPageRoutingModule {}
