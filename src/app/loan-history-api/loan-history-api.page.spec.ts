import { ComponentFixture, TestBed } from '@angular/core/testing';
import { LoanHistoryApiPage } from './loan-history-api.page';

describe('LoanHistoryApiPage', () => {
  let component: LoanHistoryApiPage;
  let fixture: ComponentFixture<LoanHistoryApiPage>;

  beforeEach(() => {
    fixture = TestBed.createComponent(LoanHistoryApiPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
