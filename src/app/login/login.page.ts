import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage {
  loginForm: FormGroup;
  passwordType: string = 'password';
  passwordShown: boolean = false;
  errorMessage: string = '';

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private storage: Storage
  ) {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
    });
  }

  navigateToSignUp() {
    this.router.navigate(['/signup']);
  }

  async login() {
    if (this.loginForm.valid) {
      const email = this.loginForm.get('email')?.value;
      const password = this.loginForm.get('password')?.value;

      if (email && password) {
        const keys = await this.storage.keys();
        let validLogin = false;

        for (const key of keys) {
          if (key.startsWith('userData_')) {
            const storedUserData = await this.storage.get(key);
            if (
              storedUserData.email === email &&
              storedUserData.password === password
            ) {
              validLogin = true;
              break;
            }
          }
        }

        if (validLogin) {
          await this.storage.set('isLoggedIn', true);
          this.router.navigate(['/dash-board', email]);
        } else {
          this.errorMessage = 'Invalid email or password';
        }
      } else {
        this.errorMessage = 'Email or password is empty';
      }
    }
  }

  togglePassword() {
    this.passwordShown = !this.passwordShown;
    this.passwordType = this.passwordShown ? 'text' : 'password';
  }
}
