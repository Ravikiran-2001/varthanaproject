import { Injectable } from '@angular/core';
import * as jwt from 'jsonwebtoken';
import axios from 'axios';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  constructor() {}

  getToken(RmLoginid: number, secrate: string): string {
    const token = jwt.sign({ RmLoginid }, secrate);
    return token;
  }

  async getLoanHistory(token: string): Promise<any> {
    try {
      const response = await axios.get(
        'http://65.2.107.120:8080/rmvarthanaloan/getloanhistory',
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      return response.data;
    } catch (error) {
      throw new Error('Failed to fetch loan history');
    }
  }
}
