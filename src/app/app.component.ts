import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage-angular';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit {
  constructor(private storage: Storage) {}
  async ngOnInit() {
    await this.storage.create();
    await this.loadAllUserData();
  }

  async loadAllUserData() {
    const keys = await this.storage.keys();
    for (const key of keys) {
      if (key.startsWith('userData_')) {
        const userData = await this.storage.get(key);
        console.log(`User data for key ${key}:`, userData);
      }
    }
  }
}
