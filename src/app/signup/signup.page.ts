import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Storage } from '@ionic/storage-angular';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage {
  userData = {
    email: '',
    name: '',
    phone_Number: '',
    gender: '',
    password: '',
    date: new Date(),
  };

  signUpForm: FormGroup;

  passwordType: string = 'password';
  passwordShown: boolean = false;

  passwordType2: string = 'password';
  passwordShown2: boolean = false;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private http: HttpClient,
    private storage: Storage
  ) {
    this.signUpForm = this.formBuilder.group(
      {
        email: ['', [Validators.required, Validators.email]],
        name: ['', [Validators.required]],
        phone: ['', [Validators.required, Validators.pattern('[0-9 ]{10}')]],
        password: ['', [Validators.required, Validators.minLength(6)]],
        confirmPassword: ['', Validators.required],
      },
      { validator: this.passwordMatchValidator }
    );
  }

  signUp() {
    if (this.signUpForm.valid) {
      const emailControl = this.signUpForm.get('email');
      if (emailControl) {
        const email = emailControl.value;
        this.router.navigate(['/dash-board', email]);
      }
    }
  }

  navigateToLogin() {
    this.router.navigate(['']);
  }

  togglePassword() {
    this.passwordShown = !this.passwordShown;
    this.passwordType = this.passwordShown ? 'text' : 'password';
  }

  togglePassword2() {
    this.passwordShown2 = !this.passwordShown2;
    this.passwordType2 = this.passwordShown2 ? 'text' : 'password';
  }

  passwordMatchValidator(formGroup: FormGroup) {
    const passwordControl = formGroup.get('password');
    const confirmPasswordControl = formGroup.get('confirmPassword');

    if (passwordControl && confirmPasswordControl) {
      if (passwordControl.value !== confirmPasswordControl.value) {
        confirmPasswordControl.setErrors({ passwordError: true });
      } else {
        confirmPasswordControl.setErrors(null);
      }
    }
  }

  genderSelected(event: any) {
    this.userData.gender = event.detail.value;
    this.userData.date = new Date();
  }

  async saveUserData() {
    const timestamp = new Date().getTime().toString();
    await this.storage.set('userData_' + timestamp, this.userData);
  }
}
