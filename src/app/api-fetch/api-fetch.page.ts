import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-api-fetch',
  templateUrl: './api-fetch.page.html',
  styleUrls: ['./api-fetch.page.scss'],
})
export class ApiFetchPage implements OnInit {
  signupDetails: any[] = [];

  constructor(private http: HttpClient) {}

  ngOnInit(): void {
    this.fetchSignupDetails();
  }

  fetchSignupDetails() {
    this.http
      .get<any[]>('https://jsonplaceholder.typicode.com/users')
      .subscribe((data) => {
        this.signupDetails = data;
      });
  }
}
