import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ApiFetchPage } from './api-fetch.page';

const routes: Routes = [
  {
    path: '',
    component: ApiFetchPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ApiFetchPageRoutingModule {}
