import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ApiFetchPage } from './api-fetch.page';

describe('ApiFetchPage', () => {
  let component: ApiFetchPage;
  let fixture: ComponentFixture<ApiFetchPage>;

  beforeEach(() => {
    fixture = TestBed.createComponent(ApiFetchPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
